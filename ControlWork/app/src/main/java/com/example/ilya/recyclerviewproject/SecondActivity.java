package com.example.ilya.recyclerviewproject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by Ilya on 23.11.2015.
 */
public class SecondActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Intent intent = getIntent();

        TextView tvTitle = (TextView) findViewById(R.id.title);
        TextView tvSubtitle = (TextView) findViewById(R.id.subtitle);
        TextView tvMainText = (TextView) findViewById(R.id.main_text);

        tvTitle.setText(intent.getStringExtra("Title"));
        tvSubtitle.setText(intent.getStringExtra("Subtitle"));
        tvMainText.setText(intent.getStringExtra("MainText"));
    }
}
