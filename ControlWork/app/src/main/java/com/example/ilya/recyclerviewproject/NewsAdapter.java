package com.example.ilya.recyclerviewproject;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Ilya on 22.11.2015.
 */
public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {

    private ArrayList<News> news;
    private Context mContext;

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView title;
        private TextView subtitle;
        private TextView mainText;
        private TextView likesCount;
        private Button like;
        private Button dislike;

        public ViewHolder(View itemView) {
            super(itemView);
            this.title = (TextView) itemView.findViewById(R.id.title);
            this.subtitle = (TextView) itemView.findViewById(R.id.subtitle);
            this.mainText = (TextView) itemView.findViewById(R.id.main_text);
            this.likesCount = (TextView) itemView.findViewById(R.id.likes_count);
            this.like = (Button) itemView.findViewById(R.id.like);
            this.dislike = (Button) itemView.findViewById(R.id.dislike);

        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, SecondActivity.class);
            intent.putExtra("Title", title.getText());
            intent.putExtra("Subtitle", subtitle.getText());
            intent.putExtra("MainText", mainText.getText());
            mContext.startActivity(intent);
        }
    }

    public NewsAdapter(ArrayList<News> news, Context context){
        this.news = news;
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        v.setOnClickListener(vh);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        News newsItem = news.get(position);
        holder.title.setText(newsItem.getTitle());
        holder.subtitle.setText(newsItem.getSubtitle());
        holder.mainText.setText(newsItem.getMainText());
        holder.likesCount.setText(newsItem.getLikesCount() + "");
        final int POS = position;
        final TextView tvLikesCount = holder.likesCount;
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentLikes;
                if(v.getId() == R.id.like){
                    currentLikes = incLikesCount(POS);
                    tvLikesCount.setText(Integer.toString(currentLikes));
                }else{
                    currentLikes = decLikesCount(POS);
                    tvLikesCount.setText(Integer.toString(currentLikes));
                }
                if(currentLikes > 0){
                    tvLikesCount.setTextColor(Color.GREEN);
                }else{
                    if(currentLikes == 0){
                        tvLikesCount.setTextColor(Color.GRAY);
                    }else{
                        tvLikesCount.setTextColor(Color.RED);
                    }
                }

            }
        };
        holder.like.setOnClickListener(listener);
        holder.dislike.setOnClickListener(listener);
    }

    @Override
    public int getItemCount() {
        return news.size();
    }

    private int incLikesCount(int position){
        News newsItem = news.get(position);
        int likes = newsItem.getLikesCount();
        newsItem.setLikesCount(++likes);
        return likes;
    }

    private int decLikesCount(int position){
        News newsItem = news.get(position);
        int likes = newsItem.getLikesCount();
        newsItem.setLikesCount(--likes);
        return likes;
    }


}
