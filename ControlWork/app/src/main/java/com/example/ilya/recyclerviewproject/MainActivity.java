package com.example.ilya.recyclerviewproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<News> newsList = new ArrayList<>();
        newsList.add(new News("News1", "some news", "Somthing about news"));
        newsList.add(new News("News2", "some news", "Somthing new about news"));
        newsList.add(new News("News3", "some news", "Some news"));
        newsList.add(new News("News4", "some news", "Somthing about news"));
        newsList.add(new News("News5", "some news", "Somthing about news"));

        RecyclerView rv = (RecyclerView) findViewById(R.id.rv);
        rv.setAdapter(new NewsAdapter(newsList, this));
        rv.setLayoutManager(new LinearLayoutManager(this));
    }
}
