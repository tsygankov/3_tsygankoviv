package com.example.ilya.recyclerviewproject;

/**
 * Created by Ilya on 22.11.2015.
 */
public class News {
    private String title;
    private String subtitle;
    private String mainText;
    private int likesCount;

    public News(String title, String subtitle, String mainText) {
        this.title = title;
        this.subtitle = subtitle;
        this.mainText = mainText;
        this.likesCount = 0;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getMainText() {
        return mainText;
    }

    public void setMainText(String mainText) {
        this.mainText = mainText;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(int likeCount) {
        this.likesCount = likeCount;
    }
}
