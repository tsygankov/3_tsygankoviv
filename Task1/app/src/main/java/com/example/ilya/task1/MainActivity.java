package com.example.ilya.task1;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final Calculator calculator = new Calculator();

        final TextView tv_display = (TextView) findViewById(R.id.display);

        Button[] btn_number = new Button[10];
        int[] buttonIds = {R.id.btn_0, R.id.btn_1,
                R.id.btn_2, R.id.btn_3,
                R.id.btn_4, R.id.btn_5,
                R.id.btn_6, R.id.btn_7,
                R.id.btn_8, R.id.btn_9};
        OnClickListener numberListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = ((Button) findViewById(v.getId())).getText().toString();
                if(!calculator.isFirstNumber()){
                    tv_display.setText("");
                }
                if(calculator.addNumber(text)) {
                    if(calculator.isFirstNumber()){
                        tv_display.setText(tv_display.getText() + text);
                    }else{
                        tv_display.setText(text);
                    }

                }


            }
        };
        for(int i = 0; i < 10; i++){
            btn_number[i] = (Button) findViewById(buttonIds[i]);
            btn_number[i].setOnClickListener(numberListener);
        }

        OnClickListener operationListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = ((Button) v).getText().toString();
                int result = calculator.makeOperation(text.charAt(0));
                if(result < -99999999 || result > 999999999){
                    tv_display.setText("Too big number");
                    calculator.clear();
                }else {
                    tv_display.setText(result + " " + text + " ");
                }
            }
        };
        Button btn_plus = (Button) findViewById(R.id.btn_plus);
        btn_plus.setOnClickListener(operationListener);
        Button btn_minus = (Button) findViewById(R.id.btn_minus);
        btn_minus.setOnClickListener(operationListener);

        OnClickListener equalListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                int result = calculator.makeOperation();

                if(result < -99999999 || result > 999999999){
                    tv_display.setText("Too big number");
                    calculator.clear();
                }else {
                    tv_display.setText("" + result);
                }
            }
        };
        Button btn_equal = (Button) findViewById(R.id.btn_equal);
        btn_equal.setOnClickListener(equalListener);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
