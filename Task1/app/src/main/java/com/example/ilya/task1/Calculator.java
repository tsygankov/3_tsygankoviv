package com.example.ilya.task1;

/**
 * Created by Ilya on 15.11.2015.
 */
public class Calculator {
    private StringBuilder firstNumber;
    private StringBuilder lastNumber;
    private char lastOperation;
    private boolean wasOperation;

    Calculator(){
        this.firstNumber = new StringBuilder("");
        this.lastNumber = new StringBuilder("");
        this.wasOperation = false;
    }

    public boolean wasOperation(){
        return wasOperation;
    }

    public boolean addNumber(String num){
        if(wasOperation){
            if(lastNumber.length() < 9){
                lastNumber.append(num);
                return true;
            }else{
                return false;
            }

        }else{
            if(firstNumber.length() < 9){
                firstNumber.append(num);
                return true;
            }else{
                return false;
            }
        }

    }

    public int makeOperation(){
        String firstNum = firstNumber.toString();
        String lastNum = lastNumber.toString();
        char operation = lastOperation;
        clear();
        if(firstNum.equals("")){
            return 0;
        }
        if(lastNum.equals("")) {
            return Integer.parseInt(firstNum);
        }
        int num1 = Integer.parseInt(firstNum);
        int num2 = Integer.parseInt(lastNum);
        switch (operation) {
            case '+':
                return num1 + num2;
            case '-':
                return num1 - num2;
            default:
                return 0; //or throw IOException
        }

    }

    public int makeOperation(char operation){
        int result = makeOperation();
        firstNumber = new StringBuilder(Integer.toString(result));
        wasOperation = true;
        lastOperation = operation;
        return result;
    }

    public boolean isFirstNumber(){
        return !firstNumber.toString().equals("");
    }

    public void clear(){
        wasOperation = false;
        firstNumber = new StringBuilder("");
        lastNumber = new StringBuilder("");
    }
}
