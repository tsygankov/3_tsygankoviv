package com.itis.androidlab.fragmentsaffiche.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.itis.androidlab.fragmentsaffiche.R;
import com.itis.androidlab.fragmentsaffiche.models.Cinema;

import java.util.List;

/**
 * Created by Ilya on 04.12.2015.
 */
public class CinemaAdapter extends RecyclerView.Adapter<CinemaAdapter.ViewHolder> {

    private Context mContext;
    private List<Cinema> mCinemaList;

    public CinemaAdapter(Context mContext, List<Cinema> mCinemaList) {
        this.mContext = mContext;
        this.mCinemaList = mCinemaList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.cinema_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Cinema cinemaItem = mCinemaList.get(position);
        holder.mCinemaName.setText(cinemaItem.getName());
        holder.mCinemaAddress.setText(cinemaItem.getAddress());
    }

    @Override
    public int getItemCount() {
        return mCinemaList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private TextView mCinemaName;
        private TextView mCinemaAddress;

        public ViewHolder(View itemView) {
            super(itemView);
            this.mCinemaName = (TextView)itemView.findViewById(R.id.tv_cinema_name);
            this.mCinemaAddress = (TextView)itemView.findViewById(R.id.tv_cinema_address);
        }
    }
}
