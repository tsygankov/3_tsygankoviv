package com.itis.androidlab.fragmentsaffiche;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.itis.androidlab.fragmentsaffiche.fragments.CinemaFragment;
import com.itis.androidlab.fragmentsaffiche.fragments.FilmChooserFragment;
import com.itis.androidlab.fragmentsaffiche.fragments.FilmDetailsFragment;
import com.itis.androidlab.fragmentsaffiche.models.Cinema;
import com.itis.androidlab.fragmentsaffiche.models.Film;

public class MainActivity extends AppCompatActivity implements FilmChooserFragment.FilmChooserProcessor,
        FilmDetailsFragment.FilmDetailProcessor{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onFilmChosen(Film film) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FilmDetailsFragment filmDetailsFragment = new FilmDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(FilmDetailsFragment.FILM, film);
        filmDetailsFragment.setArguments(bundle);

        if (fragmentManager.findFragmentById(R.id.film_details) != null) {
            ft.replace(R.id.film_details, filmDetailsFragment, FilmDetailsFragment.class.getSimpleName());
            ft.commit();
        } else {
            ft.add(R.id.film_details, filmDetailsFragment, FilmDetailsFragment.class.getSimpleName());
            ft.commit();
        }
    }

    @Override
    public void onCinema() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        CinemaFragment cinemaFragment = new CinemaFragment();

        if (fragmentManager.findFragmentById(R.id.film_details) != null) {
            ft.replace(R.id.film_details, cinemaFragment, CinemaFragment.class.getSimpleName());
            ft.commit();
        } else {
            ft.add(R.id.film_details, cinemaFragment, CinemaFragment.class.getSimpleName());
            ft.commit();
        }
    }

    @Override
    public void showCinemasWithFilm(int filmId) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        CinemaFragment cinemaFragment = new CinemaFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(CinemaFragment.FILM_ID, filmId);
        cinemaFragment.setArguments(bundle);

        if(fragmentManager.findFragmentById(R.id.film_details) != null){
            transaction.replace(R.id.film_details, cinemaFragment, CinemaFragment.class.getSimpleName());
        }else{
            transaction.add(R.id.film_details, cinemaFragment, CinemaFragment.class.getSimpleName());
        }
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
