package com.itis.androidlab.fragmentsaffiche.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itis.androidlab.fragmentsaffiche.R;
import com.itis.androidlab.fragmentsaffiche.adapters.CinemaAdapter;
import com.itis.androidlab.fragmentsaffiche.models.Cinema;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ilya on 03.12.2015.
 */
public class CinemaFragment extends Fragment {
    public static final String FILM_ID = "film_id";
    private RecyclerView mCinemasRecyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cinema, container, false);
        Bundle args = getArguments();
        if(args != null){
            Log.d("LOL", "TOP");
            initViews(view, args.getInt(FILM_ID));
        }else{
            Log.d("LOL", "KEK");
            initViews(view);
        }

        return view;
    }

    private void initViews(View view){
        mCinemasRecyclerView = (RecyclerView) view.findViewById(R.id.rv_cinema);
        List<Cinema> mCinemas = readFilmsFromJson();
        mCinemasRecyclerView.setAdapter(new CinemaAdapter(this.getContext(), mCinemas));
        mCinemasRecyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
    }

    private void initViews(View view, int filmId){
        mCinemasRecyclerView = (RecyclerView) view.findViewById(R.id.rv_cinema);
        List<Cinema> mCinemas = readFilmsFromJson();
        mCinemas = filterCinemas(mCinemas, filmId);
        mCinemasRecyclerView.setAdapter(new CinemaAdapter(this.getActivity(), mCinemas));
        mCinemasRecyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
    }

    private List<Cinema> filterCinemas(List<Cinema> cinemaList, int filmId){
        List<Cinema> filteredList = new ArrayList<>();
        for(Cinema cinema: cinemaList){
            if (cinema.checkFilmInCinema(filmId)){

                filteredList.add(cinema);
            }
        }
        return filteredList;
    }

    private List<Cinema> readFilmsFromJson(){
        ObjectMapper mapper = new ObjectMapper();
        try {
            Cinema.CinemaArray cinemaArray = mapper.readValue(getActivity().getAssets().open("cinemas.json"),
                    Cinema.CinemaArray.class);
            return cinemaArray.getItems();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
