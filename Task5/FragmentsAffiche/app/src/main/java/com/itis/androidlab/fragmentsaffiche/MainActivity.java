package com.itis.androidlab.fragmentsaffiche;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.itis.androidlab.fragmentsaffiche.fragments.CinemasFragment;
import com.itis.androidlab.fragmentsaffiche.fragments.FilmsListFragment;
import com.itis.androidlab.fragmentsaffiche.fragments.ViewPagerFragment;
import com.itis.androidlab.fragmentsaffiche.help.Parser;
import com.itis.androidlab.fragmentsaffiche.models.Cinema;

import java.io.IOException;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_header_container);
        navigationView.setNavigationItemSelectedListener(this);
        Log.d("!!!", "all");

    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (item.getItemId()){
            case R.id.nav_films:
                ViewPagerFragment viewPagerFragment = new ViewPagerFragment();
                transaction.replace(R.id.fl_container, viewPagerFragment, ViewPagerFragment.class.getSimpleName());
                break;
            case R.id.nav_cinemas:
                Parser parser = new Parser();
                try {
                    CinemasFragment cinemasFragment = new CinemasFragment(parser.parseCinemasFromJson(getAssets()
                            .open("cinemas.json")));
                    transaction.replace(R.id.fl_container, cinemasFragment, CinemasFragment.class.getSimpleName());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        transaction.commit();
        return super.onOptionsItemSelected(item);
    }
}
