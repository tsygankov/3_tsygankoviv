package com.itis.androidlab.fragmentsaffiche;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.itis.androidlab.fragmentsaffiche.help.GenresTranslator;
import com.itis.androidlab.fragmentsaffiche.models.Cinema;
import com.itis.androidlab.fragmentsaffiche.models.Film;
import com.itis.androidlab.fragmentsaffiche.models.Genre;

import java.util.List;

/**
 * Created by Ilya on 07.12.2015.
 */
public class FilmDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView mTitleTextView;
    private TextView mDateTextView;
    private TextView mDescriptionTextView;
    private TextView mDirectorTextView;
    private TextView mGenresTextView;
    private Button mGetCinemasButton;
    private Film film;
    public final static String FILM_EXTRA = "film";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film_details);
        Intent intent = getIntent();
        film = intent.getParcelableExtra(FILM_EXTRA);
        initViews();
        fillViews(film);

    }

    private void fillViews(Film film) {
        mTitleTextView.setText(film.getTitle());
        mDateTextView.setText(String.format(getResources().getString(R.string.film_date), film.getDate()));
        mDescriptionTextView.setText(String.format(getResources().getString(R.string.film_description), film.getDescription()));
        mDirectorTextView.setText(String.format(getResources().getString(R.string.film_director), film.getDirector()));
        GenresTranslator genresTranslator = new GenresTranslator();
        StringBuilder stringBuilder = new StringBuilder("");
        for(Genre genre: film.getGenres()){
            stringBuilder.append(genresTranslator.translateToRussian(genre));
            stringBuilder.append(", ");
        }
        String genres = stringBuilder.substring(0, stringBuilder.length() - 1);
        mGenresTextView.setText(String.format(getResources().getString(R.string.film_genres), genres));
        mGetCinemasButton.setOnClickListener(this);
    }

    private void initViews() {
        mTitleTextView = (TextView) findViewById(R.id.tv_title);
        mDateTextView = (TextView) findViewById(R.id.tv_date);
        mDescriptionTextView = (TextView) findViewById(R.id.tv_description);
        mDirectorTextView = (TextView) findViewById(R.id.tv_director);
        mGenresTextView = (TextView) findViewById(R.id.tv_genres);
        mGetCinemasButton = (Button) findViewById(R.id.btn_where);
    }



    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, CinemasActivity.class);
        intent.putExtra(CinemasActivity.FILM, film);
        this.startActivity(intent);
    }
}
