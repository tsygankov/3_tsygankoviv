package com.itis.androidlab.fragmentsaffiche.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;

public enum Genre{
    FANTASTIC("fantastic"),
    COMEDY("comedy"),
    FANTASY("fantasy"),
    ACTION("action"),
    ADVENTURE("adventure"),
    CARTOON("cartoon"),
    DRAMA("drama");

    private String mValue;

    Genre(String value) {
        mValue = value;
    }

    public static Genre getEnum(String value){
        for (Genre g : values()){
            if(g.toString().equalsIgnoreCase(value)){
                return g;
            }
        }
        throw new IllegalArgumentException();
    }

    @JsonValue
    @Override
    public String toString() {
        return mValue;
    }


}
