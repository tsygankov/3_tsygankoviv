package com.itis.androidlab.fragmentsaffiche.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.itis.androidlab.fragmentsaffiche.R;
import com.itis.androidlab.fragmentsaffiche.help.Parser;
import com.itis.androidlab.fragmentsaffiche.models.Cinema;

import java.io.IOException;
import java.util.List;

public class CinemasFragment extends Fragment {
    private List<Cinema> cinemas;
    private RecyclerView recyclerView;

    public CinemasFragment(){
        super();
    }

    public CinemasFragment(List<Cinema> cinemaList){
        super();
        cinemas = cinemaList;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        recyclerView = (RecyclerView) inflater.inflate(R.layout.recycler_view_layout, container, false);
        if(cinemas != null){
            fillView();
        }
        return recyclerView;
    }

    public void setList(List<Cinema> cinemaList){
        cinemas = cinemaList;
        fillView();
    }

    private void fillView(){
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        recyclerView.setAdapter(new CinemaAdapter(this.getContext(), cinemas));
    }

    class CinemaAdapter extends RecyclerView.Adapter<CinemaAdapter.ViewHolder>{

        private List<Cinema> cinemas;
        private Context mContext;

        public CinemaAdapter(Context context, List<Cinema> cinemaList){
            mContext = context;
            cinemas = cinemaList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View itemView = inflater.inflate(R.layout.cinema_item, parent, false);
            ViewHolder viewHolder = new ViewHolder(itemView);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            Cinema cinema = cinemas.get(position);
            holder.mNameTextView.setText(cinema.getName());
            holder.mAddressTextView.setText(cinema.getAddress());
        }

        @Override
        public int getItemCount() {
            return cinemas.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder{
            public TextView mNameTextView;
            public TextView mAddressTextView;

            public ViewHolder(View itemView) {
                super(itemView);
                mNameTextView = (TextView) itemView.findViewById(R.id.cinema_name);
                mAddressTextView = (TextView) itemView.findViewById(R.id.cinema_address);
            }
        }
    }
}
