package com.itis.androidlab.fragmentsaffiche.help;

import android.util.Log;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itis.androidlab.fragmentsaffiche.models.Cinema;
import com.itis.androidlab.fragmentsaffiche.models.Film;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ilya on 07.12.2015.
 */
public class Parser {
    private List<Film> films;
    private List<Cinema> cinemas;

    public List<Film> parseFilmFromJson(InputStream stream) throws IOException {
        if(films == null) {
            ObjectMapper objectMapper = new ObjectMapper();
            Film.FilmArray filmArray = objectMapper.readValue(stream, new TypeReference<Film.FilmArray>() {
            });
            films = filmArray.getItems();
        }
        return films;

    }

    public List<Cinema> parseCinemasFromJson(InputStream stream) throws IOException {
        if(cinemas == null) {
            ObjectMapper objectMapper = new ObjectMapper();
            Cinema.CinemaArray cinemaArray = objectMapper.readValue(stream, new TypeReference<Cinema.CinemaArray>() {
            });
            cinemas = cinemaArray.getItems();
        }
        Log.d("lol", cinemas.toString());
        return cinemas;
    }

    public List<Cinema> parseFilmCinemasJson(InputStream stream, long filmId) throws IOException {
        List<Cinema> result = new ArrayList<>();
        List<Cinema> cinemaList = parseCinemasFromJson(stream);
        for(Cinema cinema : cinemaList){
            if(cinema.getFilms().contains(filmId)){
                result.add(cinema);
            }
        }
        return result;
    }
}
