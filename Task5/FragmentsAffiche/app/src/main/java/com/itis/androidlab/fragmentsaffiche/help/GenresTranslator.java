package com.itis.androidlab.fragmentsaffiche.help;

import com.itis.androidlab.fragmentsaffiche.models.Genre;

import java.util.HashMap;

/**
 * Created by Ilya on 07.12.2015.
 */
public class GenresTranslator {
    HashMap<Genre, String> russianGenreMap;

    public GenresTranslator(){
        initRussianGenreMap();
    }

    private void initRussianGenreMap(){
        russianGenreMap = new HashMap<>();
        russianGenreMap.put(Genre.FANTASTIC, "Фантастика");
        russianGenreMap.put(Genre.COMEDY, "Комедия");
        russianGenreMap.put(Genre.FANTASY, "Фэнтези");
        russianGenreMap.put(Genre.ADVENTURE, "Приключение");
        russianGenreMap.put(Genre.CARTOON, "Мультфильм");
        russianGenreMap.put(Genre.DRAMA, "Драма");
        russianGenreMap.put(Genre.ACTION, "Экшн");
    }

    public String translateToRussian(Genre genre){
        return russianGenreMap.get(genre);
    }
}
