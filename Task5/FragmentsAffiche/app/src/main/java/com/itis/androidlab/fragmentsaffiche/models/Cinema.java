package com.itis.androidlab.fragmentsaffiche.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ilya on 03.12.2015.
 */
public class Cinema implements Parcelable {
    private String name;
    private String address;
    @JsonProperty("films")
    private ArrayList<Long> films;

    public Cinema(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ArrayList<Long> getFilms() {
        return films;
    }

    public void setFilms(ArrayList<Long> films) {
        this.films = films;
    }

    public boolean checkFilmInCinema(Film film){
        return checkFilmInCinema(film.getId());
    }

    public boolean checkFilmInCinema(long id){
        return films.contains(id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(address);
        dest.writeList(films);
    }

    protected Cinema(Parcel parcel){
        this.name = parcel.readString();
        this.address = parcel.readString();
        this.films = parcel.readArrayList(null);
    }

    public static Creator<Cinema> CREATOR = new Creator<Cinema>(){

        @Override
        public Cinema createFromParcel(Parcel source) {
            return new Cinema(source);
        }

        @Override
        public Cinema[] newArray(int size) {
            return new Cinema[size];
        }


    };

    public static class CinemaArray{
        public List<Cinema> items;

        public List<Cinema> getItems() {
            return items;
        }

        public void setItems(List<Cinema> items) {
            this.items = items;
        }
    }
}