package com.itis.androidlab.fragmentsaffiche.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.itis.androidlab.fragmentsaffiche.R;
import com.itis.androidlab.fragmentsaffiche.help.Parser;
import com.itis.androidlab.fragmentsaffiche.models.Film;
import com.itis.androidlab.fragmentsaffiche.models.Genre;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ilya on 08.12.2015.
 */
public class ViewPagerFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.view_pager_fragment, container, false);
        ViewPager viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        setupViewPager(viewPager, view);
        return view;
    }

    private void setupViewPager(ViewPager viewPager, View view) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        List<Film> films = getFilmListFromFile();
        if(films != null){
            adapter.addFragment(new FilmsListFragment(films), "Все");
            adapter.addFragment(new FilmsListFragment(filterFilmsByGenre(films, Genre.ACTION)),
                    "Экшн");
            adapter.addFragment(new FilmsListFragment(filterFilmsByGenre(films, Genre.FANTASTIC)),
                    "Фантастика");
            adapter.addFragment(new FilmsListFragment(filterFilmsByGenre(films, Genre.CARTOON)),
                    "Мультфильмы");
        }

        viewPager.setAdapter(adapter);
        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    private List<Film> getFilmListFromFile(){
        Parser parser = new Parser();
        try {
            return parser.parseFilmFromJson(getActivity().getAssets().open("films.json"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private List<Film> filterFilmsByGenre(List<Film> films, Genre genre){
        List<Film> result = new ArrayList<>();
        for (Film film : films){
            if (film.getGenres().contains(genre)){
                result.add(film);
            }
        }
        return result;
    }

    public static class ViewPagerAdapter extends FragmentPagerAdapter {

        private List<Fragment> fragments;
        private List<String> titles;

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
            fragments = new ArrayList<>();
            titles = new ArrayList<>();
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles.get(position);
        }

        public void addFragment(Fragment fragment, String title){
            fragments.add(fragment);
            titles.add(title);
        }
    }
}
