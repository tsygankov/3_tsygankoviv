package com.itis.androidlab.fragmentsaffiche;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.itis.androidlab.fragmentsaffiche.fragments.CinemasFragment;
import com.itis.androidlab.fragmentsaffiche.help.Parser;
import com.itis.androidlab.fragmentsaffiche.models.Cinema;
import com.itis.androidlab.fragmentsaffiche.models.Film;

import java.io.IOException;
import java.util.List;

/**
 * Created by Ilya on 08.12.2015.
 */
public class CinemasActivity extends AppCompatActivity {

    public final static String FILM = "film";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cinema);
        CinemasFragment cinemasFragment = (CinemasFragment) getSupportFragmentManager().findFragmentById(R.id.cinema_fragment);
        Parser parser = new Parser();
        Intent intent = getIntent();
        Film film = intent.getParcelableExtra(FILM);
        try {
            cinemasFragment.setList(parser.parseFilmCinemasJson(getAssets().open("cinemas.json"), film.getId()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
