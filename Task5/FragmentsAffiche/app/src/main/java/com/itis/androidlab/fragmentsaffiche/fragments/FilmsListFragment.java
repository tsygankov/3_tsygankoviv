package com.itis.androidlab.fragmentsaffiche.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.itis.androidlab.fragmentsaffiche.FilmDetailsActivity;
import com.itis.androidlab.fragmentsaffiche.R;
import com.itis.androidlab.fragmentsaffiche.models.Film;

import java.util.ArrayList;
import java.util.List;

public class FilmsListFragment extends Fragment {

    private List<Film> films;

    public FilmsListFragment(){
        super();
        films = new ArrayList<>();
    }

    public FilmsListFragment(List<Film> films){
        this.films = films;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        RecyclerView recyclerView = (RecyclerView) inflater.inflate(R.layout.recycler_view_layout, container, false);
        setupRecyclerView(recyclerView);
        return recyclerView;
    }

    private void setupRecyclerView(RecyclerView recyclerView){
        recyclerView.setAdapter(new FilmsAdapter(this.getContext(), films));
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
    }

    public static class FilmsAdapter extends RecyclerView.Adapter<FilmsAdapter.ViewHolder>{

        private List<Film> mFilms;
        private Context mContext;

        public FilmsAdapter(Context context, List<Film> films){
            mFilms = films;
            mContext = context;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View itemView = inflater.inflate(R.layout.film_item, parent, false);
            ViewHolder holder = new ViewHolder(itemView);
            return holder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            holder.mTitleTextView.setText(mFilms.get(position).getTitle());
            holder.mDateTextView.setText(mFilms.get(position).getDate());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, FilmDetailsActivity.class);
                    intent.putExtra(FilmDetailsActivity.FILM_EXTRA, mFilms.get(position));
                    mContext.startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return mFilms.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder{
            public TextView mTitleTextView;
            public TextView mDateTextView;

            public ViewHolder(View itemView) {
                super(itemView);
                mTitleTextView = (TextView) itemView.findViewById(R.id.film_title);
                mDateTextView = (TextView) itemView.findViewById(R.id.film_date);
            }
        }
    }
}
