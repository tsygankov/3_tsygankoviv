package com.example.ilya.task2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Ilya on 19.11.2015.
 */
public class NewsVHAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<News> news;

    class ViewHolder{
        public ImageView ivImg;
        public TextView tvTitle;
        public TextView tvBody;
        public TextView tvDate;
    }

    public NewsVHAdapter(Context context, ArrayList<News> news) {
        this.context = context;
        this.news = news;
    }

    @Override
    public int getCount() {
        return news.size();
    }

    @Override
    public Object getItem(int position) {
        return news.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        if(rowView == null){
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.news_item, parent, false);

            ViewHolder vh = new ViewHolder();
            vh.ivImg = (ImageView) rowView.findViewById(R.id.img);
            vh.tvTitle = (TextView) rowView.findViewById(R.id.title);
            vh.tvBody = (TextView) rowView.findViewById(R.id.body);
            vh.tvDate = (TextView) rowView.findViewById(R.id.date);
            rowView.setTag(vh);
        }

        ViewHolder holder = (ViewHolder) rowView.getTag();

        News newsItem = news.get(position);
        holder.ivImg.setImageResource(newsItem.getImageResource());
        holder.tvTitle.setText(newsItem.getTitle());
        holder.tvBody.setText(newsItem.getBody());
        holder.tvDate.setText(newsItem.getDate());

        return rowView;
    }
}
