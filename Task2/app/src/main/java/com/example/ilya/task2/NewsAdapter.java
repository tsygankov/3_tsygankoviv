package com.example.ilya.task2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Ilya on 19.11.2015.
 */
public class NewsAdapter extends BaseAdapter{
    private Context context;
    private ArrayList<News> news;

    public NewsAdapter(Context context, ArrayList<News> news) {
        this.context = context;
        this.news = news;
    }

    @Override
    public int getCount() {
        return news.size();
    }

    @Override
    public Object getItem(int position) {
        return news.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.news_item, parent, false);

        ImageView ivImg = (ImageView) view.findViewById(R.id.img);
        TextView tvTitle = (TextView) view.findViewById(R.id.title);
        TextView tvBody = (TextView) view.findViewById(R.id.body);
        TextView tvDate = (TextView) view.findViewById(R.id.date);


        News newsItem = news.get(position);
        ivImg.setImageResource(newsItem.getImageResource());
        tvTitle.setText(newsItem.getTitle());
        tvBody.setText(newsItem.getBody());
        tvDate.setText(newsItem.getDate());

        return view;
    }
}
