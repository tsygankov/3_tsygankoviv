package com.example.ilya.task2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        News[] news = {new News("Android 6.0", "Some text", "08.05.13", R.drawable.ic_format_float_left),
                new News("iOS 9", "Some text", "08.05.13", R.drawable.ic_format_float_left),
                new News("Windows 10 update!", "Some text", "08.05.13", R.drawable.ic_format_float_left),
                new News("Nexus 7 update", "Some text", "08.05.13", R.drawable.ic_format_float_left),
                new News("News", "Some text", "08.05.13", R.drawable.ic_format_float_left)};
        ArrayList<News> newsList = new ArrayList<News>(Arrays.asList(news));
        NewsVHAdapter newsVHAdapter = new NewsVHAdapter(this, newsList);


        ListView lv = (ListView) findViewById(R.id.lv);
        Log.d("TAG", "" + newsVHAdapter.getCount());
        lv.setAdapter(newsVHAdapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                News news = (News) parent.getItemAtPosition(position);
                Log.d("TAG", "clicked");
                Toast.makeText(MainActivity.this, news.getTitle(), Toast.LENGTH_LONG).show();
            }
        });



    }
}
