package com.example.ilya.task2;

/**
 * Created by Ilya on 19.11.2015.
 */
public class News {
    private String title;
    private String body;
    private String date;
    private int imageResource;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public News(String title, String body, String date, int imageResource) {

        this.title = title;
        this.body = body;
        this.date = date;
        this.imageResource = imageResource;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public int getImageResource() {
        return imageResource;
    }

    public void setImageResource(int imageResource) {
        this.imageResource = imageResource;
    }
}
