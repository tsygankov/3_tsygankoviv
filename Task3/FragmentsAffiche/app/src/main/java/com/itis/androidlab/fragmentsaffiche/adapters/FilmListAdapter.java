package com.itis.androidlab.fragmentsaffiche.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.itis.androidlab.fragmentsaffiche.R;
import com.itis.androidlab.fragmentsaffiche.fragments.FilmChooserFragment;
import com.itis.androidlab.fragmentsaffiche.models.Film;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Ilya on 02.12.2015.
 */
public class FilmListAdapter extends RecyclerView.Adapter<FilmListAdapter.ViewHolder>  {
    private final String APP_PREFERENCES = "IdsPreferences";
    private final String IDS = "ids";
    private List<Film> films;
    private List<Integer> idsMustWatch;
    private Context mContext;

    public FilmListAdapter(List filmList, Context context){
        this.films = filmList;
        this.mContext = context;
        this.idsMustWatch = new ArrayList<>();
        SharedPreferences preferences = mContext.getSharedPreferences(APP_PREFERENCES,
                Context.MODE_PRIVATE);
        if(preferences.contains(IDS)){
            Set<String> ids = preferences.getStringSet(IDS, new HashSet<String>());
            for(String id : ids){
                try{
                    idsMustWatch.add(Integer.parseInt(id));
                }catch (NumberFormatException e){
                    Log.d("oops", "PARSING ERROR");
                }
            }
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.film_chooser_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        view.setOnClickListener(viewHolder);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Film filmItem = films.get(position);
        holder.mTitleTextView.setText(filmItem.getTitle());
        if(idsMustWatch.contains(films.get(position).getId())){
            holder.itemView.setBackgroundColor(Color.YELLOW);
        }
    }

    @Override
    public int getItemCount() {
        return films.size();
    }

    private int findFilmPositionById(int id){
        for(int i = 0; i < films.size(); i++){
            if(films.get(i).getId() == id){
                return i;
            }
        }
        return -1;
    }

    public void watchFilm(int id){
        if(!idsMustWatch.contains(id)){
            idsMustWatch.add(id);
            int position = findFilmPositionById(id);
            if(position != -1){
                notifyItemChanged(position);
            }

        }

    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView mTitleTextView;

        public ViewHolder(View itemView){
            super(itemView);
            this.mTitleTextView = (TextView) itemView.findViewById(R.id.film_title_item);
        }

        @Override
        public void onClick(View v) {
            Film film = films.get(getAdapterPosition());
            FilmChooserFragment.FilmChooserProcessor processor =
                    (FilmChooserFragment.FilmChooserProcessor) mContext;
            processor.onFilmChosen(film);
        }
    }

    public void saveInfo(){
        SharedPreferences preferences = mContext
                .getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        HashSet<String> savingSet = new HashSet<>();
        for(int id : idsMustWatch){
            savingSet.add(Integer.toString(id));
        }
        SharedPreferences.Editor editor = preferences.edit();
        editor.putStringSet(IDS, savingSet);
        editor.apply();
    }

    public interface OnFilmClick{
        void onFilmClick(Film film);
    }
}
