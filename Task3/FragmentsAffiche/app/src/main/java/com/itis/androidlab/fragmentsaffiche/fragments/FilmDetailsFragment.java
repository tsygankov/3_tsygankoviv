package com.itis.androidlab.fragmentsaffiche.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.itis.androidlab.fragmentsaffiche.R;
import com.itis.androidlab.fragmentsaffiche.models.Film;

public class FilmDetailsFragment extends Fragment implements View.OnClickListener {

    private int id;
    private TextView mTitleTextView;
    private TextView mDateTextView;
    private TextView mDescriptionTextView;
    private TextView mDirectorTextView;
    private TextView mActorsTextView;
    private TextView mBudgetTextView;
    private Button mWatchButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_film_details, container, false);
        initViews(view);

        return view;
    }

    private void initViews(View view) {
        mTitleTextView = (TextView) view.findViewById(R.id.film_title);
        mDateTextView = (TextView) view.findViewById(R.id.film_date);
        mDescriptionTextView = (TextView) view.findViewById(R.id.film_description);
        mDirectorTextView = (TextView) view.findViewById(R.id.film_director);
        mActorsTextView = (TextView) view.findViewById(R.id.film_actors);
        mBudgetTextView = (TextView) view.findViewById(R.id.film_budget);
        mWatchButton = (Button) view.findViewById(R.id.btn_watch);
        mWatchButton.setOnClickListener(this);
    }

    public void setFilm(Film film) {
        id = film.getId();
        mTitleTextView.setText(film.getTitle());
        mDateTextView.setText(film.getDate());
        mDescriptionTextView.setText(film.getDescription());
        mDirectorTextView.setText(String.format(getResources().getString(R.string.film_director), film.getDirector()));
        mActorsTextView.setText(String.format(getResources().getString(R.string.film_actors), film.getActors()));
        mBudgetTextView.setText(String.format(getResources().getString(R.string.film_budget), film.getBudget()));
        mWatchButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        FilmAddable processor = (FilmAddable) getActivity();
        processor.addFilmToMustWatch(id);

    }

    public interface FilmAddable{
        public void addFilmToMustWatch(int id);
    }
}
