package com.itis.androidlab.fragmentsaffiche.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itis.androidlab.fragmentsaffiche.R;
import com.itis.androidlab.fragmentsaffiche.adapters.FilmListAdapter;
import com.itis.androidlab.fragmentsaffiche.models.Film;

import java.io.IOException;
import java.util.List;

public class FilmChooserFragment extends Fragment {
    private RecyclerView rv;
    private final String APP_PREFERENCES = "appPreferences";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_film_chooser, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        List<Film> mFilms = readFilmsFromJson();
        rv = (RecyclerView) view.findViewById(R.id.rv_films);
        rv.setAdapter(new FilmListAdapter(mFilms, this.getContext()));
        rv.setLayoutManager(new LinearLayoutManager(this.getContext()));
    }

    private List<Film> readFilmsFromJson() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            Film.FilmArray filmArray = mapper.readValue(getActivity().getAssets().open("films.json"),
                    new TypeReference<Film.FilmArray>() {
                    });
            return filmArray.getItems();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void addFilmToMustWatch(int id){
        FilmListAdapter adapter = (FilmListAdapter) rv.getAdapter();
        adapter.watchFilm(id);
        SharedPreferences preferences = getActivity().getSharedPreferences(APP_PREFERENCES,
                Context.MODE_PRIVATE);

    }

    public interface FilmChooserProcessor {
        void onFilmChosen(Film film);
    }

    @Override
    public void onPause() {
        super.onPause();
        FilmListAdapter filmListAdapter = (FilmListAdapter) rv.getAdapter();
        filmListAdapter.saveInfo();
    }
}
